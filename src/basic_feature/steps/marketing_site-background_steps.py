from behave import given, when, then
import src.common


@then("I see the Clifton Suspension bridge background")
def step_impl(context):
    element = context.driver.find_element_by_xpath("/html/body/div[1]/div/div[4]/div")
    expected_background = "Clifton_Suspension_Bridge_two.jpg"
    actual_css = element.value_of_css_property("background")
    assert expected_background in actual_css
