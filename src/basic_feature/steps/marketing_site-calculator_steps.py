from behave import given, when, then
from selenium.webdriver.common.action_chains import ActionChains
from time import sleep
import src.common


@when("I select London Portsmouth pricing")
def step_impl(context):
    portsmouth_element = '//*[@id="lon-p"]/img'
    driver = context.driver
    with src.common.wait_for_element(driver, portsmouth_element):
        # After element is present, wait a second for animations to finish...
        sleep(1)
    portsmouth_pricing_link = context.driver.find_element_by_xpath(
            '//*[@id="lon-p"]/img')
    portsmouth_pricing_link.click()


@when("I select 2000MHz of CPU")
def step_impl(context):
    source_element = context.driver.find_element_by_xpath(
            '//*[@id="server-list"]/div/div/div/div[4]/div[2]/div[1]/div[2]/div/div')
    action = ActionChains(context.driver)
    action.drag_and_drop_by_offset(source_element,77 ,0).perform()


@when("I select 1024MB of RAM")
def step_impl(context):
    source_element = context.driver.find_element_by_xpath(
            '//*[@id="server-list"]/div/div/div/div[4]/div[2]/div[2]/div[2]/div/div')
    action = ActionChains(context.driver)
    action.drag_and_drop_by_offset(source_element,90 ,0).perform()


@when("I select a static IP address")
def step_impl(context):
    element = context.driver.find_element_by_xpath(
            '//*[@id="server-list"]/div/div/div/div[4]/div[2]/div[3]/div/div[1]/label/input')
    if not element.is_selected():
        element.click()


@when("I select a firewall")
def step_impl(context):
    element = context.driver.find_element_by_xpath(
            '//*[@id="server-list"]/div/div/div/div[4]/div[2]/div[4]/div/div[1]/label/input')
    if not element.is_selected():
        element.click()


@when("I select a 100GB HDD")
def step_impl(context):
    source_element = context.driver.find_element_by_xpath(
            '//*[@id="server-list"]/div/div/div/div[5]/div[4]/div[1]/div[2]/div/div')
    action = ActionChains(context.driver)
    action.drag_and_drop_by_offset(source_element,62 ,0).perform()


@when("I select 10GB of data transfer")
def step_impl(context):
    source_element = context.driver.find_element_by_xpath(
            '//*[@id="calculator"]/div/div[4]/div/div/div[3]/div[3]/div[1]/div[2]/div/div')
    action = ActionChains(context.driver)
    action.drag_and_drop_by_offset(source_element,15 ,0).perform()


@then("I see a £42.40 monthly cost")
def step_impl(context):
    current_price = context.driver.find_element_by_xpath(
            '//*[@id="pricing"]/div/div/div[2]/h4[2]/div').text
    assert "£42.40" in current_price
