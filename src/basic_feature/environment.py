from datetime import datetime

def after_step(context, step):
    # If step failed print out screen grab
    if step.status == 'failed':
        now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S--')
        step_str = context.scenario.name + "-" + step.name
        filename = "fail_screenshots/" + now + step_str + ".png"
        context.driver.get_screenshot_as_file(filename)

def after_scenario(context, scenario):
    context.driver.quit()
