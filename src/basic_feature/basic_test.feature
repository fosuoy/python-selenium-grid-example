Feature: Testing the dynamic features of the ElasticHosts marketing site
    A test suite showing the possibilities of automated testing for ElasticHosts
    Showing two functional / working tests and one failing test
    The failing test should print out a screen grab showing which assertion was
    incorrect

    @marketing_site
    Scenario: Navigating to the UK marketing site - do I see the Clifton Suspension bridge?
        Given I load the main ElasticHosts marketing site
        When I navigate to the UK site
        Then I see the Clifton Suspension bridge background
    
    @failing_test
    @pricing_calculator
    @marketing_site
    Scenario: Given a spec on the pricing calculator - is a wrong price displayed?
        Given I load the main ElasticHosts marketing site
        When I navigate to the pricing calculator
        And I select London Portsmouth pricing
        And I select 2000MHz of CPU
        And I select 1024MB of RAM
        And I select a static IP address
        And I select a 100GB HDD
        And I select 10GB of data transfer
        Then I see a £42.40 monthly cost


    @pricing_calculator
    @marketing_site
    Scenario: Given a spec on the pricing calculator - is the price correct?
        Given I load the main ElasticHosts marketing site
        When I navigate to the pricing calculator
        And I select London Portsmouth pricing
        And I select 2000MHz of CPU
        And I select 1024MB of RAM
        And I select a static IP address
        And I select a firewall
        And I select a 100GB HDD
        And I select 10GB of data transfer
        Then I see a £42.40 monthly cost
