from behave import given, when, then
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.common.exceptions import NoSuchElementException, ElementNotVisibleException
from time import time, sleep


@given("I load the main ElasticHosts marketing site")
def step_impl(context):
    driver_present = getattr(context, "driver", None)
    if not driver_present:
        selenium_hub = 'http://localhost:4444/wd/hub'
        context.driver = webdriver.Remote(command_executor=selenium_hub,
                desired_capabilities=DesiredCapabilities.CHROME)
    context.driver.get("https://www.elastichosts.com/")


@when("I navigate to the UK site")
def step_impl(context):
    # Click on UK flag at top of page
    context.driver.find_element_by_xpath(
            "/html/body/div[1]/div/div[2]/ul[3]/li[4]/a/img").click()


@when("I navigate to the pricing calculator")
def step_impl(context):
    # Click on pricing tab at top of page
    context.driver.find_element_by_xpath(
            "/html/body/div[1]/div/nav/section[1]/ul/li[2]/a").click()


class wait_for_element(object):
    def __init__(self, driver, xpath):
        self.driver = driver
        self.xpath = xpath


    def wait_for(self, condition_function):
        start_time = time()
        while time() < start_time + 3:
            if condition_function():
                return True
            else:
                sleep(0.1)
        raise Exception(
            'Timeout waiting for {}'.format(condition_function.__name__)
        )


    def element_is_present(self):
        try:
            self.driver.find_element_by_xpath(self.xpath)
        except NoSuchElementException:
            return False
        return True


    def __enter__(self):
        return self


    def __exit__(self, *_):
        self.wait_for(self.element_is_present)
