# A sample Selenium setup using behave for BDD


This is a sample testing setup to showcase moving regression testing / QA over
from a manual process to Selenium to integrate testing into a CI/CD pipeline.

## Running the tests...

A working docker installation is required for running the test - this is for
Selenium grid.


Although you can now run selenium tests in the background of your local computer
using Chrome headless, the idea is that this allows a centralized Selenium server
to be set up and be interegrated with a current workflow / delivery process.


Create a virtualenv:

`virtualenv -p python3 env`


This setup require python3 so create a virtual environment with python3...

`(env)..$ pip3 install -r requirements.txt`


Start up the required docker images for Selenium grid:

`(env)../<PROJECT_DIR>$ docker-compose up -d`


You should now have a running hub plus one worker node running google-chrome.

To run all the tests run:

`(env)../<PROJECT_DIR>$ behave src/*/`


Included is a failing test which you can run using the tag @failing_test:

`(env)../<PROJECT_DIR>$ behave --tags=failing_test src/*/`

The failing test should output a screengrab of the failed screen into:

`<PROJECT_DIR>/fail_screenshots/`


After finishing testing you can bring down the environment using:

`(env)../<PROJECT_DIR>$ docker-compose down`

## The example tests

The project is set up with some common features under src/common.py - otherwise
features are split up by directory.

A test which checks an element is present in current CSS - and a test which drags
/drops items on a page and clicking items should cover the majority of
test cases.

Otherwise the BDD set up should allow the conversion of currently written tests
to be done quickly / easily.
